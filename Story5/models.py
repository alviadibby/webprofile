from django.db import models
from datetime import datetime, date

CATEGORY_CHOICES = (
    ('design','Graphic Design'),
    ('userInterface', 'User Interface'),
    ('others', 'Others'),
)

class Appointment(models.Model):
    name = models.CharField(max_length = 20)
    date = models.DateField(default = datetime.now)
    time = models.TimeField()
    activity = models.CharField(max_length=20)
    place = models.CharField(max_length = 20)
    category = models.CharField (max_length = 20, choices=CATEGORY_CHOICES)


