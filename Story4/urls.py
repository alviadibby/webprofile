from django.urls import path

from . import views

urlpatterns = [
    path('', views.landing, name='landing'),
    path('landing/', views.landing, name='landing'),
    path('profile/', views.profile, name='profile'),
    path('experiences/', views.experiences, name='experiences'),
    path('achievements/', views.achievements, name='achievements'),
    path('contact/', views.contact, name='contact'),
]
