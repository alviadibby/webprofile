from django.shortcuts import render
from django.http import HttpResponse

def landing(request):
    return render(request, "page1.html")

def profile(request):
    return render(request, "page2.html")

def experiences(request):
    return render(request, "page3.html")

def achievements(request):
    return render(request, "page4.html")

def contact(request):
    return render(request, "page5.html")

