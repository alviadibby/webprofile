from django.urls import path

from . import views

urlpatterns = [
    path('', views.appointment, name='appointment'),
    path('create/', views.appointment_create, name='appointment_create'),
    path('clear/<int:id>/', views.appointment_clear, name='appointment_clear'),
]