from django.shortcuts import render, redirect
from .models import Appointment
from . import forms
from django.http import HttpResponse


def appointment(request):
    appointments = Appointment.objects.all().order_by('date')
    return render(request, 'page6.html', {'appointments' : appointments})

def appointment_create(request):
    if request.method == 'POST':
        form = forms.AppointmentForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('appointment')
    else:
        form = forms.AppointmentForm()
    return render(request, 'page7.html', {'form': form})

def appointment_clear(request, id):
    if request.method == 'POST':
        Appointment.objects.filter(id=id).delete()
        return redirect('appointment')
    else:
        return HttpResponse("/GET not allowed")