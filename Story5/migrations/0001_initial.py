# Generated by Django 2.2.5 on 2019-10-06 05:36

import datetime
from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Appointment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=20)),
                ('date', models.DateField(default=datetime.datetime.now)),
                ('time', models.TimeField(default=django.utils.timezone.now)),
                ('activity', models.CharField(max_length=20)),
                ('place', models.CharField(max_length=20)),
                ('category', models.CharField(max_length=20)),
            ],
        ),
    ]
